﻿using System;
using System.Data;
using System.IO;
using System.Xml;
using Xml_From_File;
using Xml2CSharp;
using System.Xml.Serialization;
using System.Text.RegularExpressions;
using System.Linq;
using System.Net;
using System.Text;
using System.Collections.Generic;
using System.Diagnostics;
using ActiveUp.Net.Mail;

namespace XmlFromFile
{
    class Program
    {
        private static readonly string folderPath = "Sanctionlist";
        static void Main(string[] args)
        {
            ReadNotification();
        }

        public static void ReadNotification()
        {
            var mailRepository = new MailRepository(
                                    "imap.gmail.com",
                                    993,
                                    true,
                                    "gpl.masupdate@gmail.com",
                                    "Ktmnepal@1"
                                );

            var emailList = mailRepository.GetUnreadMails("inbox");

            foreach (Message email in emailList)
            {
                if (email.From.Email == "")
                {
                    MASUpdate();
                }
            }
        }

        public static void MASUpdate()
        {
            string[] Urllist = new string[] {
                "https://scsanctions.un.org/dprk/",
                "https://scsanctions.un.org/drc/",
                "https://scsanctions.un.org/iran/",
                "https://scsanctions.un.org/libya/",
                "https://scsanctions.un.org/somalia/",
                "https://scsanctions.un.org/south-sudan/",
                "https://scsanctions.un.org/taliban/",
                "https://scsanctions.un.org/al-qaida/",
                "https://scsanctions.un.org/yemen/",
                "https://scsanctions.un.org/sudan/"
            };
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            int i = 0;
            foreach (var Link in Urllist)
            {
                var xml = DownloadString(Link);
                File.WriteAllText(folderPath + "\\" + i + ".xml", xml);
                i++;
            }

            using (TestEntities db = new TestEntities())
            {
                db.Database.ExecuteSqlCommand("delete from [terrorist_sdn_entry] where sdn_type='MAS_LIST'");

                List<terrorist_sdn_entry> SanctionLists = new List<terrorist_sdn_entry>();
                foreach (string file in Directory.EnumerateFiles(folderPath, "*.xml"))
                {
                    Debug.WriteLine(file);
                    SanctionLists = ReadXml(file);
                    if (SanctionLists.Count > 0)
                    {
                        SaveSdn(SanctionLists, db);
                        db.sdnUpdateLogs.Add(new sdnUpdateLog
                        {
                            SDN_TYPE = SanctionLists.FirstOrDefault().sdn_type + "-" + SanctionLists.FirstOrDefault().Country,
                            TOTAL_RECORDS = SanctionLists.Count,
                            UPDATED_DATE = DateTime.Now
                        });
                        db.SaveChanges();
                    }
                }
            }
        }

        public static string DownloadString(string address)
        {
            string text;
            using (var client = new WebClient())
            {
                text = client.DownloadString(address);
            }
            return text;
        }
        public static List<terrorist_sdn_entry> ReadXml(string FileFullPath)
        {
            List<terrorist_sdn_entry> sdn = new List<terrorist_sdn_entry>();
            FileInfo fs = new FileInfo(FileFullPath);
            string new_file = fs.Directory + "\\" + fs.Name + "_new.xml";
            using (MemoryStream ms = new MemoryStream())
            {
                var FileData = File.ReadAllText(FileFullPath);
                FileData = Regex.Replace(FileData, "&(?!amp;)", "&amp;");
                File.WriteAllText(new_file, FileData);
                XmlSerializer serializer = new XmlSerializer(typeof(CONSOLIDATED_LIST));
                using (var Fs = new FileStream(new_file, FileMode.Open))
                {
                    var i = (CONSOLIDATED_LIST)serializer.Deserialize(Fs);
                    foreach (var d in i.INDIVIDUALS.INDIVIDUAL)
                        sdn.Add(new terrorist_sdn_entry
                        {
                            sdn_entry_id = Convert.ToInt32(d.DATAID),
                            first_name = d.FIRST_NAME ?? "",
                            last_name = (d.SECOND_NAME ?? "") + " " + (d.THIRD_NAME ?? "") + " " + (d.FOURTH_NAME ?? ""),
                            full_name = (d.FIRST_NAME ?? "") + " " + (d.SECOND_NAME ?? "") + " " + (d.THIRD_NAME ?? "") + " " + (d.FOURTH_NAME ?? ""),
                            Gender = d.GENDER ?? "",
                            sdn_type = "MAS_LIST",
                            uploaded_by = 171,
                            uploaded_date = DateTime.Now,
                            isNew = "y",
                            remarks = d.COMMENTS1 ?? "",
                            Country = d.COUNTRY ?? (d.UN_LIST_TYPE ?? ""),
                            nationality = d.NATIONALITY == null ? "" : d.NATIONALITY.VALUE,
                            pep_full_name = (d.FIRST_NAME ?? "") + " " + (d.SECOND_NAME ?? "") + " " + (d.THIRD_NAME ?? "") + " " + (d.FOURTH_NAME ?? "")
                        });

                    foreach (var d in i.ENTITIES.ENTITY)
                        sdn.Add(new terrorist_sdn_entry
                        {
                            sdn_entry_id = Convert.ToInt32(d.DATAID),
                            first_name = d.FIRST_NAME ?? "",
                            sdn_type = "MAS_LIST",
                            uploaded_by = 171,
                            uploaded_date = DateTime.Now,
                            isNew = "y",
                            remarks = d.COMMENTS1 ?? "",
                            Country = d.COUNTRY ?? (d.UN_LIST_TYPE ?? ""),
                            pep_full_name = d.FIRST_NAME ?? ""
                        });
                }
            }
            File.Delete(new_file);
            File.Delete(fs.Directory + "\\" + fs.Name);
            return sdn;
        }
        public static void SaveSdn(List<terrorist_sdn_entry> SdnList, TestEntities db)
        {
            foreach (var d in SdnList)
            {
                db.terrorist_sdn_entry.Add(d);
                db.SaveChanges();
            }
        }
    }
}
