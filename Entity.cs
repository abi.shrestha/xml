﻿using System;
using System.Xml.Serialization;
using System.Collections.Generic;
namespace XmlFromFile.Entities
{
	[XmlRoot(ElementName = "LIST_TYPE")]
	public class LIST_TYPE
	{
		[XmlElement(ElementName = "VALUE")]
		public string VALUE { get; set; }
	}

	[XmlRoot(ElementName = "LAST_DAY_UPDATED")]
	public class LAST_DAY_UPDATED
	{
		[XmlElement(ElementName = "VALUE")]
		public string VALUE { get; set; }
	}

	[XmlRoot(ElementName = "ENTITY_ALIAS")]
	public class ENTITY_ALIAS
	{
		[XmlElement(ElementName = "QUALITY")]
		public string QUALITY { get; set; }
		[XmlElement(ElementName = "ALIAS_NAME")]
		public string ALIAS_NAME { get; set; }
	}

	[XmlRoot(ElementName = "ENTITY_ADDRESS")]
	public class ENTITY_ADDRESS
	{
		[XmlElement(ElementName = "STREET")]
		public string STREET { get; set; }
		[XmlElement(ElementName = "CITY")]
		public string CITY { get; set; }
		[XmlElement(ElementName = "COUNTRY")]
		public string COUNTRY { get; set; }
	}

	[XmlRoot(ElementName = "ENTITY")]
	public class ENTITY
	{
		[XmlElement(ElementName = "DATAID")]
		public string DATAID { get; set; }
		[XmlElement(ElementName = "VERSIONNUM")]
		public string VERSIONNUM { get; set; }
		[XmlElement(ElementName = "FIRST_NAME")]
		public string FIRST_NAME { get; set; }
		[XmlElement(ElementName = "UN_LIST_TYPE")]
		public string UN_LIST_TYPE { get; set; }
		[XmlElement(ElementName = "REFERENCE_NUMBER")]
		public string REFERENCE_NUMBER { get; set; }
		[XmlElement(ElementName = "LISTED_ON")]
		public string LISTED_ON { get; set; }
		[XmlElement(ElementName = "COMMENTS1")]
		public string COMMENTS1 { get; set; }
		[XmlElement(ElementName = "LIST_TYPE")]
		public LIST_TYPE LIST_TYPE { get; set; }
		[XmlElement(ElementName = "LAST_DAY_UPDATED")]
		public LAST_DAY_UPDATED LAST_DAY_UPDATED { get; set; }
		[XmlElement(ElementName = "ENTITY_ALIAS")]
		public List<ENTITY_ALIAS> ENTITY_ALIAS { get; set; }
		[XmlElement(ElementName = "ENTITY_ADDRESS")]
		public ENTITY_ADDRESS ENTITY_ADDRESS { get; set; }
		[XmlElement(ElementName = "SORT_KEY")]
		public string SORT_KEY { get; set; }
		[XmlElement(ElementName = "SORT_KEY_LAST_MOD")]
		public string SORT_KEY_LAST_MOD { get; set; }
	}

}
