﻿using System;
using System.Xml.Serialization;
using System.Collections.Generic;
namespace Xml2CSharp
{
    [Serializable, XmlRoot(ElementName = "DESIGNATION")]
    public class DESIGNATION
    {
        [XmlElement(ElementName = "VALUE")]
        public List<string> VALUE { get; set; }
    }

    [Serializable, XmlRoot(ElementName = "NATIONALITY")]
    public class NATIONALITY
    {
        [XmlElement(ElementName = "VALUE")]
        public string VALUE { get; set; }
    }

    [Serializable, XmlRoot(ElementName = "LIST_TYPE")]
    public class LIST_TYPE
    {
        [XmlElement(ElementName = "VALUE")]
        public string VALUE { get; set; }
    }

    [Serializable, XmlRoot(ElementName = "LAST_DAY_UPDATED")]
    public class LAST_DAY_UPDATED
    {
        [XmlElement(ElementName = "VALUE")]
        public List<string> VALUE { get; set; }
    }

    [Serializable, XmlRoot(ElementName = "INDIVIDUAL_ALIAS")]
    public class INDIVIDUAL_ALIAS
    {
        [XmlElement(ElementName = "QUALITY")]
        public string QUALITY { get; set; }
        [XmlElement(ElementName = "ALIAS_NAME")]
        public string ALIAS_NAME { get; set; }
        [XmlElement(ElementName = "DATE_OF_BIRTH")]
        public string DATE_OF_BIRTH { get; set; }
    }

    [Serializable, XmlRoot(ElementName = "INDIVIDUAL_ADDRESS")]
    public class INDIVIDUAL_ADDRESS
    {
        [XmlElement(ElementName = "COUNTRY")]
        public string COUNTRY { get; set; }
        [XmlElement(ElementName = "CITY")]
        public string CITY { get; set; }
    }

    [Serializable, XmlRoot(ElementName = "INDIVIDUAL_DATE_OF_BIRTH")]
    public class INDIVIDUAL_DATE_OF_BIRTH
    {
        [XmlElement(ElementName = "TYPE_OF_DATE")]
        public string TYPE_OF_DATE { get; set; }
        [XmlElement(ElementName = "DATE")]
        public string DATE { get; set; }
        [XmlElement(ElementName = "YEAR")]
        public string YEAR { get; set; }
        [XmlElement(ElementName = "FROM_YEAR")]
        public string FROM_YEAR { get; set; }
        [XmlElement(ElementName = "TO_YEAR")]
        public string TO_YEAR { get; set; }
    }

    [Serializable, XmlRoot(ElementName = "INDIVIDUAL_DOCUMENT")]
    public class INDIVIDUAL_DOCUMENT
    {
        [XmlElement(ElementName = "TYPE_OF_DOCUMENT")]
        public string TYPE_OF_DOCUMENT { get; set; }
        [XmlElement(ElementName = "NUMBER")]
        public string NUMBER { get; set; }
        [XmlElement(ElementName = "DATE_OF_ISSUE")]
        public string DATE_OF_ISSUE { get; set; }
        [XmlElement(ElementName = "NOTE")]
        public string NOTE { get; set; }
        [XmlElement(ElementName = "COUNTRY_OF_ISSUE")]
        public string COUNTRY_OF_ISSUE { get; set; }
    }

    [Serializable, XmlRoot(ElementName = "INDIVIDUAL")]
    public class INDIVIDUAL
    {
        [XmlElement(ElementName = "DATAID")]
        public string DATAID { get; set; }
        [XmlElement(ElementName = "VERSIONNUM")]
        public string VERSIONNUM { get; set; }

        [XmlElement(ElementName = "FIRST_NAME")]
        public string FIRST_NAME { get; set; }
        [XmlElement(ElementName = "SECOND_NAME")]
        public string SECOND_NAME { get; set; }
        [XmlElement(ElementName = "THIRD_NAME")]
        public string THIRD_NAME { get; set; }
        [XmlElement(ElementName = "FOURTH_NAME")]
        public string FOURTH_NAME { get; set; }
        [XmlElement(ElementName = "UN_LIST_TYPE")]
        public string UN_LIST_TYPE { get; set; }
        [XmlElement(ElementName = "REFERENCE_NUMBER")]
        public string REFERENCE_NUMBER { get; set; }
        [XmlElement(ElementName = "LISTED_ON")]
        public string LISTED_ON { get; set; }

        [XmlElement(ElementName = "COMMENTS1")]
        public string COMMENTS1 { get; set; }

        [XmlElement(ElementName = "DESIGNATION")]
        public DESIGNATION DESIGNATION { get; set; }
        [XmlElement(ElementName = "NATIONALITY")]
        public NATIONALITY NATIONALITY { get; set; }
        [XmlElement(ElementName = "COUNTRY")]
        public string COUNTRY { get; set; }
        [XmlElement(ElementName = "LIST_TYPE")]
        public LIST_TYPE LIST_TYPE { get; set; }
        [XmlElement(ElementName = "LAST_DAY_UPDATED")]
        public LAST_DAY_UPDATED LAST_DAY_UPDATED { get; set; }
        [XmlElement(ElementName = "INDIVIDUAL_ALIAS")]
        public List<INDIVIDUAL_ALIAS> INDIVIDUAL_ALIAS { get; set; }
        [XmlElement(ElementName = "INDIVIDUAL_ADDRESS")]
        public INDIVIDUAL_ADDRESS INDIVIDUAL_ADDRESS { get; set; }
        [XmlElement(ElementName = "INDIVIDUAL_DATE_OF_BIRTH")]
        public List<INDIVIDUAL_DATE_OF_BIRTH> INDIVIDUAL_DATE_OF_BIRTH { get; set; }
        [XmlElement(ElementName = "INDIVIDUAL_DOCUMENT")]
        public List<INDIVIDUAL_DOCUMENT> INDIVIDUAL_DOCUMENT { get; set; }
        [XmlElement(ElementName = "SORT_KEY")]
        public string SORT_KEY { get; set; }
        [XmlElement(ElementName = "SORT_KEY_LAST_MOD")]
        public string SORT_KEY_LAST_MOD { get; set; }
        [XmlElement(ElementName = "GENDER")]
        public string GENDER { get; set; }
        [XmlElement(ElementName = "INDIVIDUAL_PLACE_OF_BIRTH")]
        public INDIVIDUAL_PLACE_OF_BIRTH INDIVIDUAL_PLACE_OF_BIRTH { get; set; }
    }

    [Serializable, XmlRoot(ElementName = "INDIVIDUAL_PLACE_OF_BIRTH")]
    public class INDIVIDUAL_PLACE_OF_BIRTH
    {
        [XmlElement(ElementName = "CITY")]
        public string CITY { get; set; }
        [XmlElement(ElementName = "COUNTRY")]
        public string COUNTRY { get; set; }
        [XmlElement(ElementName = "STATE_PROVINCE")]
        public string STATE_PROVINCE { get; set; }
    }

    [Serializable, XmlRoot(ElementName = "INDIVIDUALS")]
    public class INDIVIDUALS
    {
        [XmlElement(ElementName = "INDIVIDUAL")]
        public List<INDIVIDUAL> INDIVIDUAL { get; set; }
    }

    [Serializable, XmlRoot(ElementName = "ENTITY_ALIAS")]
    public class ENTITY_ALIAS
    {
        [XmlElement(ElementName = "QUALITY")]
        public string QUALITY { get; set; }
        [XmlElement(ElementName = "ALIAS_NAME")]
        public string ALIAS_NAME { get; set; }
    }

    [Serializable, XmlRoot(ElementName = "ENTITY_ADDRESS")]
    public class ENTITY_ADDRESS
    {
        [XmlElement(ElementName = "CITY")]
        public string CITY { get; set; }
        [XmlElement(ElementName = "COUNTRY")]
        public string COUNTRY { get; set; }
        [XmlElement(ElementName = "STREET")]
        public string STREET { get; set; }
        [XmlElement(ElementName = "STATE_PROVINCE")]
        public string STATE_PROVINCE { get; set; }
        [XmlElement(ElementName = "ZIP_CODE")]
        public string ZIP_CODE { get; set; }
    }

    [Serializable, XmlRoot(ElementName = "ENTITY")]
    public class ENTITY
    {
        [XmlElement(ElementName = "DATAID")]
        public string DATAID { get; set; }
        [XmlElement(ElementName = "VERSIONNUM")]
        public string VERSIONNUM { get; set; }
        [XmlElement(ElementName = "FIRST_NAME")]
        public string FIRST_NAME { get; set; }
        [XmlElement(ElementName = "UN_LIST_TYPE")]
        public string UN_LIST_TYPE { get; set; }
        [XmlElement(ElementName = "REFERENCE_NUMBER")]
        public string REFERENCE_NUMBER { get; set; }
        [XmlElement(ElementName = "LISTED_ON")]
        public string LISTED_ON { get; set; }
        [XmlElement(ElementName = "COMMENTS1")]
        public string COMMENTS1 { get; set; }
        [XmlElement(ElementName = "COUNTRY")]
        public string COUNTRY { get; set; }
        [XmlElement(ElementName = "LIST_TYPE")]
        public LIST_TYPE LIST_TYPE { get; set; }
        [XmlElement(ElementName = "LAST_DAY_UPDATED")]
        public LAST_DAY_UPDATED LAST_DAY_UPDATED { get; set; }
        [XmlElement(ElementName = "ENTITY_ALIAS")]
        public List<ENTITY_ALIAS> ENTITY_ALIAS { get; set; }
        [XmlElement(ElementName = "ENTITY_ADDRESS")]
        public List<ENTITY_ADDRESS> ENTITY_ADDRESS { get; set; }
        [XmlElement(ElementName = "SORT_KEY")]
        public string SORT_KEY { get; set; }
        [XmlElement(ElementName = "SORT_KEY_LAST_MOD")]
        public string SORT_KEY_LAST_MOD { get; set; }
    }

    [Serializable, XmlRoot(ElementName = "ENTITIES")]
    public class ENTITIES
    {
        [XmlElement(ElementName = "ENTITY")]
        public List<ENTITY> ENTITY { get; set; }
    }

    [Serializable, XmlRoot(ElementName = "CONSOLIDATED_LIST")]
    public class CONSOLIDATED_LIST
    {
        [XmlElement(ElementName = "INDIVIDUALS")]
        public INDIVIDUALS INDIVIDUALS { get; set; }
        [XmlElement(ElementName = "ENTITIES")]
        public ENTITIES ENTITIES { get; set; }
        [XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Xsi { get; set; }
        [XmlAttribute(AttributeName = "noNamespaceSchemaLocation", Namespace = "http://www.w3.org/2001/XMLSchema-instance")]
        public string NoNamespaceSchemaLocation { get; set; }
        [XmlAttribute(AttributeName = "dateGenerated")]
        public string DateGenerated { get; set; }
    }

}
