//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Xml_From_File
{
    using System;
    using System.Collections.Generic;
    
    public partial class sdnUpdateLog
    {
        public int SNO { get; set; }
        public Nullable<System.DateTime> UPDATED_DATE { get; set; }
        public Nullable<int> TOTAL_RECORDS { get; set; }
        public string SDN_TYPE { get; set; }
    }
}
