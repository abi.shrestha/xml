﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XmlFromFile.Model
{
    public class INDIVIDUALS
    {
        public List<Individual> individuals { get;set; }
    }
    public class Individual
    {
        public int sdnEntryId { get; set; }
        public String firstName { get; set; }
        public String secondName { get; set; }
        public String thirdName { get; set; }
        public String fourthName { get; set; }
        public String lastName { get; set; }
        public String fullName { get; set; }
        public String gender { get; set; }
        public String country { get; set; }
        public String aliasName { get; set; }
        public String remarks { get; set; }
        
    }
}
